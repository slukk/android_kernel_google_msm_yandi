#!/bin/bash

clear

# AK Kernel Version
BASE_AK_VER="Yandi"
VER=".v02-CAF"
AK_VER=$BASE_AK_VER$VER

# AK Variables
export LOCALVERSION="~"`echo $AK_VER`
#export CROSS_COMPILE=${HOME}/android/AK-linaro/4.8.2-2013.09.20130923/bin/arm-linux-gnueabihf-
export CROSS_COMPILE=/media/slukk/EXT4/mako_kernel43/CAF_TSM/Slukk-toolchain/4.8.2-2013.09.20130923/bin/arm-linux-gnueabihf-
export ARCH=arm
export SUBARCH=arm
export KBUILD_BUILD_USER=slukk
export KBUILD_BUILD_HOST="linux"

DATE_START=$(date +"%s")

echo " "
echo "========================"
echo "|                      |"
echo "| mako kernel by slukk |"
echo "|                      |"
echo "========================"
echo " "

MODULES_DIR=${HOME}/Alldata/mako_kernel43/CAF_TSM/anykernel/system/lib/modules
KERNEL_DIR=`pwd`
OUTPUT_DIR=${HOME}/Alldata/mako_kernel43/CAF_TSM/anykernel/zip
CWM_DIR=${HOME}/Alldata/mako_kernel43/CAF_TSM/anykernel/cwm
ZIMAGE_DIR=${HOME}/Alldata/mako_kernel43/CAF_TSM/Slukk-mako/arch/arm/boot
CWM_MOVE=${HOME}/Alldata/mako_kernel43/CAF_TSM
ZIMAGE_ANYKERNEL=${HOME}/Alldata/mako_kernel43/CAF_TSM/anykernel/cwm/kernel
ANYKERNEL_DIR=${HOME}/Alldata/mako_kernel43/CAF_TSM/anykernel

echo -e "${red}"; echo "COMPILING VERSION:"; echo -e "${blink_red}"; echo "$LOCALVERSION"; echo -e "${restore}"
echo "CROSS_COMPILE="$CROSS_COMPILE
echo "ARCH="$ARCH
echo "MODULES_DIR="$MODULES_DIR
echo "KERNEL_DIR="$KERNEL_DIR
echo "OUTPUT_DIR="$OUTPUT_DIR
echo "CWM_DIR="$CWM_DIR
echo "ZIMAGE_DIR="$ZIMAGE_DIR
echo "CWM_MOVE="$CWM_MOVE
echo "ZIMAGE_ANYKERNEL="$ZIMAGE_ANYKERNEL
echo "ANYKERNEL_DIR="$ANYKERNEL_DIR

echo -e "${green}"
echo "-------------------------"
echo "Making: TSM Mako Defconfig"
echo "-------------------------"
echo -e "${restore}"

make "mako_defconfig"
#make -j3 > /dev/null
make -j4

echo -e "${green}"
echo "-------------------------"
echo "Create: TSM Kernel and Zip"
echo "-------------------------"
echo -e "${restore}"

rm `echo $MODULES_DIR"/*"`
find $KERNEL_DIR -name '*.ko' -exec cp -v {} $MODULES_DIR \;
echo

cp -vr $ZIMAGE_DIR/zImage $ZIMAGE_ANYKERNEL
echo

cd $CWM_DIR
zip -r `echo $AK_VER`.zip *
mv  `echo $AK_VER`.zip $OUTPUT_DIR

echo -e "${green}"
echo "-------------------------"
echo "The End: TSM is Born"
echo "-------------------------"
echo -e "${restore}"

cp -vr $OUTPUT_DIR/`echo $AK_VER`.zip $CWM_MOVE
echo

cd $KERNEL_DIR

echo -e "${green}"
echo "-------------------------"
echo "Build Completed in:"
echo "-------------------------"
echo -e "${restore}"

DATE_END=$(date +"%s")
DIFF=$(($DATE_END - $DATE_START))
echo "Time: $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds."
echo
